% Black-Scholes Pricing Model
%This is the call option price function
function[C]=calloptionfunction(S,d1,K,r,T,d2,sigma) %C means the call option price 
                                                  %S means the underlying price
                                                  %r means the risk free rate
                                                  %T means the tenor 
C=S*cdf('norm',d1,0,1)-K*[e^(-rT)]*cdf('norm',d2,0,1);
    function[d1,d2]=secondaryfunction(S,K,r,T,sigma) %sigma represents the volatility of S,which is 1 here
    d1=(ln(S/K)+(r+(1/2))*T)/(T^(1/2)); %we directly plug in sigma as 1
    d2=(ln(S/K)+(r-(1/2))*T)/(T^(1/2));

%This is the put option price fuction
function[P]=primary_function(S,d1,K,r,T,d2,sigma)  %P means the put option price
P=K*(e^(-rT))*cdf('norm',-d2,0,1)-S*cdf('norm',-d1,0,1);
function[d1,d2]=secondary_function(S,K,r,T,sigma)
d1=(ln(S/K)+(r+(1/2))*T)/(T^(1/2)); %we directly plug in sigma as 1
d2=(ln(S/K)+(r-(1/2))*T)/(T^(1/2));

%This is call delta function
function[CD]=calldeltafunction(d1)  %CD means the first derivative of the call option price with respect to underlying price S
CD=cdf('norm',d1,0,1);
function[d1]=call_deltafunction(S,K,r,T,sigma)
d1=(ln(S/K)+(r+(1/2))*T)/(T^(1/2)); %we directly plug in sigma as 1

%This is put delta function
function[PD]=putdeltafunction(d1)   %PD means the first derivative of the put option price with respect to the underlying price S
PD=cdf('norm',d1,0,1)-1;
function[d1]=put_deltafunction(S,K,r,T,sigma)
d1=(ln(S/K)+(r+(1/2))*T)/(T^(1/2)); %we directly plug in sigma as 1

%This is the function of Gamma
function[Gamma]=gammafunction(S,d1,T,sigma) %Gamma means the second derivative of call option price with respect to the underlying price S
Gamma=pdf('norm',d1,0,1)/(S*(T^(1/2))); %sigma equals 1 and is plugged in
function[d1]=gamma_function(S,K,r,T,sigma)
d1=(ln(S/K)+(r+(1/2))*T)/(T^(1/2)); %we directly plug in sigma as 1

%This is the function of vega
function[vega]=vegafunction(S,d1,T)  %Vega means the first derivative of call option price with respect to the volatity of S
vega=S*pdf('norm',d1,0,1)*(T^(1/2));
function[d1]=vega_function(S,K,r,T,sigma)
d1=(ln(S/K)+(r+(1/2))*T)/(T^(1/2)); %we directly plug in sigma as 1

%This is the function of call theta
function[CT]=callthetafunction(S,d1,T,sigma,r,K,d2) %CT is the negative of first derivative of call option price with respect to time
CT=-((S*pdf('norm',d1,0,1))/(2*(T^(1/2)))-(r*K*(e^(-rT))*cdf('norm',d2,0,1)); %we have plugged in \sigma as 1
function[d1,d2]=call_theta_function(S,K,r,T,sigma) %sigma represents the volatility of S,which is 1 here
d1=(ln(S/K)+(r+(1/2))*T)/(T^(1/2)); %we directly plug in sigma as 1
d2=(ln(S/K)+(r-(1/2))*T)/(T^(1/2));  

%This is the function of put theta
function[PT]=putthetafunction(S,d1,T,sigma,r,K,d2) %PT the negative of the first derivative of put option price with respect to time
PT=-((S*pdf('norm',d1,0,1))/(2*(T^(1/2)))+r*K*(e^(-rT))*cdf('norm',-d2,0,1); %we have plugged in \sigma as 1
function[d1,d2]=put_theta_function(S,K,r,T,sigma) %sigma represents the volatility of S,which is 1 here
d1=(ln(S/K)+(r+(1/2))*T)/(T^(1/2)); %we directly plug in sigma as 1
d2=(ln(S/K)+(r-(1/2))*T)/(T^(1/2)); 

%This is the function of call rho
function[CR]=callrhofunction(K,T,r,d2)  %call rho is the first derivative of call option price with respect to risk-free rate
CR=K*T*(e^(-rT))*cdf('norm',d2,0,1);
function[d2]=call_rho_function(S,K,r,T,sigma) %sigma represents the volatility of underlying price S which is 1 here
d2=(ln(S/K)+(r-(1/2))*T)/(T^(1/2));  %we have plugged in sigma as 1

%This is the function of put rho
function[PT]=putrhofunction(K,T,r,d2)   %put rho is the first derivative of put option price with respect to risk-free rate
PT=-K*T*(e^(-rT))*cdf('norm',-d2,0,1);
    function[d2]=put_rho_function(S,K,r,T,sigma)
    d2=(ln(S/K)+(r-(1/2))*T)/(T^(1/2));  %we have plugged in sigma as 1
